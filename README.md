# Tingisweb



## Introduction

This document provides a comprehensive overview of the API for managing blog posts developed 
as a coding test assignment. The project is designed to showcase proficiency in Spring Boot, 
Java 17, 
and associated technologies for building a functional and secure Blog System.
***

# Technologies Used

The following technologies were used in the implementation of the Blog System:

    Spring Boot
    Java 17
    Spring Data JPA
    Spring Security
    PostgreSQL
    Thymeleaf
    Docker
## Functionalities

The implemented Blog System includes the following functionalities:

    Creating a Blog Post
    Retrieving all Blog Posts
    Retrieving a Single Blog Post
    Updating an Existing Blog Post
    Deleting a Blog Post

## Setup and Run Instructions
navigate: http://localhost:8080/home
To log in to the application, use the following credentials:

    Username: admin
    Password: admin

Note: The functionalities to add, delete, and update blog posts are restricted to authenticated users only. Users need to log in using the provided credentials (username: admin, password: admin) to access these operations.
Unauthorized users will not be able to perform these actions.


To run the Docker image for your Tingisweb application, you can use the following instructions:
## create Docker image

First, ensure that you have Docker installed on your machine. If not, you can download and install it from the Docker website.
Once Docker is installed, build your Docker image using the Dockerfile. Navigate to the directory containing your Dockerfile and run the following command:

docker build -t tingisweb-image .

Replace tingisweb-image with your desired image name.

After successfully building the Docker image, you can run the Docker container using the following command:

## run Docker image

docker run -p 8080:8080 tingisweb-image

This command will run the Docker container, mapping port 8080 on your local machine to port 8080 in the Docker container.

Access your Tingisweb application by opening a web browser and navigating to http://localhost:8080 or the appropriate IP address and port if you are running Docker on a remote machine.

Ensure that the necessary environment variables and configurations are set up correctly in your Docker image to ensure the smooth functioning of your Tingisweb application.
Adjust the commands as needed based on your specific Docker configuration.