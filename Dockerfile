# Start with a base image containing Java runtime
FROM openjdk:17-jdk

# Add Maintainer Info
LABEL maintainer="Mounir El kharraz <elkharraz.mounir@gmail.com"

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8080 available to the world outside this container
EXPOSE 8080

# The application's jar file
ARG JAR_FILE=target/artifacts/tingisweb.jar





# Run the jar file
ENTRYPOINT ["java","-jar","/tingiswebjar"]