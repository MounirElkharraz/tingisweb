package com.example.blog.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.ui.Model;

import com.example.blog.dto.Blog.BlogDto;
import com.example.blog.service.BlogService;

class BlogControllerTest {

    private BlogController blogController;
    private BlogService blogService;

    // Set up the necessary components before each test
    @BeforeEach
    void setUp() {
        blogService = mock(BlogService.class);
        blogController = new BlogController();
        blogController.setBlogService(blogService);
    }

    @Test
    void testGetAllBlogs() {
        // Arrange
        List<BlogDto> expectedBlogs = new ArrayList<>();
        when(blogService.findAllBlogs()).thenReturn(expectedBlogs);
        Model model = mock(Model.class);

        // Act
        String result = blogController.getAllBlogs(model);

        // Assert
        assertEquals("blog-list", result);
    }

    @Test
    void testGetBlogById() {
        // Arrange
        Long id = 1L;
        BlogDto expectedBlog = new BlogDto();
        when(blogService.findBlogById(id)).thenReturn(expectedBlog);
        Model model = mock(Model.class);

        // Act
        String result = blogController.getBlogById(id, model);

        // Assert
        assertEquals("blog-details", result);
    }

    @Test
    void testGetUpdateBlogById() {
        // Arrange
        Long id = 1L;
        BlogDto expectedBlog = new BlogDto();
        when(blogService.findBlogById(id)).thenReturn(expectedBlog);
        Model model = mock(Model.class);

        // Act
        String result = blogController.getUpdateBlogById(id, model);

        // Assert
        assertEquals("blog-details", result);
    }

    @Test
    void testUpdateBlog() {
        // Arrange
        BlogDto updateBlogRequest = new BlogDto();
        BlogDto expectedBlog = new BlogDto();
        when(blogService.updateBlog(updateBlogRequest)).thenReturn(expectedBlog);
        Model model = mock(Model.class);

        // Act
        String result = blogController.UpdateBlog(updateBlogRequest, model);

        // Assert
        assertEquals("blog-list", result);
    }

    @Test
    void testAddBlog() {
        // Arrange
        BlogDto addBlogRequest = new BlogDto();
        BlogDto newBlog = new BlogDto();
        when(blogService.createBlog(addBlogRequest)).thenReturn(newBlog);
        Model model = mock(Model.class);

        // Act
        String result = blogController.addBlog(addBlogRequest, model);

        // Assert
        assertEquals("blog-list", result);
    }

    @Test
    void testRenderFormPage() {
        // Arrange
        Model model = mock(Model.class);

        // Act
        String result = blogController.renderFormPage(model);

        // Assert
        assertEquals("add-blog", result);
    }

    @Test
    void testBlogList() {
        Model model = mock(Model.class);

        // Act
        String result = blogController.blogList(model);

        // Assert
        assertEquals("blog-list", result);
    }


}
