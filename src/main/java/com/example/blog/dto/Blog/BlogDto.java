package com.example.blog.dto.Blog;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BlogDto {
	

	 private Long id;
	
	 private String title;

	 private String content;

	 private String author;

	 private LocalDate publicationDate;

}
