package com.example.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.example.blog.dto.Blog.BlogDto;
import com.example.blog.service.BlogService;

@Controller
public class BlogController {

	@Autowired
	private BlogService blogService;


	// Setter method for blogService
	public void setBlogService(BlogService blogService) {
		this.blogService = blogService;
	}

	/**
	 * Endpoint to display all blogs
	 * @param model
	 * @return
	 */
	@GetMapping("/blogs")
	public String getAllBlogs(Model model) {

		// Retrieve all blogs from the service
		List<BlogDto> blogs = blogService.findAllBlogs();

		// Add the blogs to the model
		model.addAttribute("blog", blogs);

		// Return the view for displaying all blogs
		return "blog-list";
	}

	/**
	 * Endpoint to display a specific blog
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/blog/{id}")
	public String getBlogById(@PathVariable Long id, Model model) {

		// Find the blog by its ID using the service
		BlogDto blog = blogService.findBlogById(id);

		// Add the blog to the model
		model.addAttribute("blog", blog);

		// Return the view for displaying the blog details
		return "blog-details";
	}

	/**
	 *
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/update/{id}")
	public String getUpdateBlogById(@PathVariable Long id, Model model) {

		// Find the blog by its ID using the service
		BlogDto blog = blogService.findBlogById(id);

		// Add the blog to the model
		model.addAttribute("blog", blog);

		// Return the view for displaying the blog details
		return "blog-details";
	}

	/**
	 *
	 * @param updateBlogRequest
	 * @param model
	 * @return the view for updating blog
	 */
	@PostMapping("/updateblog/{id}")
	public String UpdateBlog(@ModelAttribute("blog") BlogDto updateBlogRequest, Model model) {

		// Find the blog by its ID using the servic
		BlogDto blog = blogService.updateBlog(updateBlogRequest);

		// Add the blog to the model
		model.addAttribute("blog", blog);

		// Return the view for displaying the blog details
		return "blog-list";
	}

	/**
	 *
	 * @param addBlogRequest
	 * @param model
	 * @return list of blog and add a new blog
	 */
	@PostMapping("/addBlog")
	public String addBlog( @ModelAttribute("blog") BlogDto addBlogRequest, Model model) {

		// Create a new blog using the service
		BlogDto newBlog = blogService.createBlog(addBlogRequest);

		// Add the new blog to the model
		model.addAttribute("blog", newBlog);

		// Return the view for the newly added blog
		return "blog-list";
	}


	/**
	 *
	 * @param model
	 * @return the view for add new blog
	 */
	@GetMapping("/addBlogView") // Replace with the actual URL for your form page
	public String renderFormPage(Model model) {
		// Add an empty BlogDto to the model
		model.addAttribute("blog", new BlogDto());
		// Return the view for the form page
		return "add-blog"; // Replace with the name of your form page template
	}

	@GetMapping("/home")
	public String blogList(Model model) {

		// Retrieve all blogs from the service
		List<BlogDto> blogs = blogService.findAllBlogs();

		// Add the blogs to the model
		model.addAttribute("blog", blogs);

		// Return the view for displaying all blogs
		return "blog-list";
	}

	@GetMapping("/")
	public String getBlogList(Model model) {

		// Retrieve all blogs from the service
		List<BlogDto> blogs = blogService.findAllBlogs();

		// Add the blogs to the model
		model.addAttribute("blog", blogs);

		// Return the view for displaying all blogs
		return "blog-list";
	}

	@GetMapping("/delete/{id}")
	public String deleteBlog(@PathVariable Long id, Model model) {
		// Call the service to delete the blog by its ID
		blogService.deleteBlog(id);

		// Retrieve all blogs from the service
		List<BlogDto> blogs = blogService.findAllBlogs();

		// Add the blogs to the model
		model.addAttribute("blog", blogs);

		// Return the view for displaying all blogs
		return "blog-list";
	}

}
