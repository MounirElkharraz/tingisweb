package com.example.blog.service;

import java.util.List;

import com.example.blog.dto.Blog.BlogDto;

public interface BlogService {
	
	public BlogDto createBlog(BlogDto addBlogRequest);
	
	public BlogDto updateBlog(BlogDto updateBlogRequest);
	
	public BlogDto findBlogById(Long id);
	
	public List<BlogDto> findAllBlogs();
	
	public void deleteBlog(Long id);

}
