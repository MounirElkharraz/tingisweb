package com.example.blog.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.blog.dto.Blog.BlogDto;
import com.example.blog.model.BlogPost;
import com.example.blog.repository.BlogRepository;
import com.example.blog.service.BlogService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class BlogServiceImpl implements BlogService {

	@Autowired
    private BlogRepository blogRepository; // Autowired repository for data access

    // Method to create a new blog
    @Override
    public BlogDto createBlog(BlogDto addBlogRequest) {
    	
        BlogPost blogPost = new BlogPost();
        
        // Copy properties from DTO to entity
        BeanUtils.copyProperties(addBlogRequest, blogPost); 
        // Set the current date 
        blogPost.setPublicationDate(LocalDate.now()); 
        // Save the BlogPost to the database
        BlogPost savedBlogPost = blogRepository.save(blogPost); 
        BlogDto response = new BlogDto();
       // Copy properties from the saved entity to DTO
        BeanUtils.copyProperties(savedBlogPost, response);
        System.out.println(response.getTitle());
        return response;
    }

    // Method to update an existing blog
    @Override
    public BlogDto updateBlog(BlogDto updateBlogRequest) {
        BlogPost existingBlog = blogRepository.findById(updateBlogRequest.getId())
                .orElseThrow(() -> new EntityNotFoundException("BlogPost not found")); // Throw exception if the BlogPost is not found
   
        // Copy properties, excluding id and publication date

        existingBlog.setTitle(updateBlogRequest.getTitle());
        existingBlog.setAuthor(updateBlogRequest.getAuthor());
        existingBlog.setContent(updateBlogRequest.getContent());
        // Save the updated blog

        BlogPost updatedBlog = blogRepository.save(existingBlog); 
        BlogDto response = new BlogDto();
        // Copy properties to the DTO
        BeanUtils.copyProperties(updatedBlog, response);
        System.out.println(updatedBlog.getTitle()+" "+response.getTitle());
        return response;
    }

    // Method to find a BlogPost by its ID
    @Override
    public BlogDto findBlogById(Long id) {
        BlogPost blog = blogRepository.findById(id)
        		// Throw exception if the BlogPost is not found
                .orElseThrow(() -> new EntityNotFoundException("BlogPost not found")); 
        BlogDto response = new BlogDto();
        // Copy properties to the DTO
        BeanUtils.copyProperties(blog, response); 
        return response;
    }

    // Method to retrieve all blogs
    @Override
    public List<BlogDto> findAllBlogs() {
    	
    	// Retrieve all blogs from the repository
        List<BlogPost> blogs = blogRepository.findAll(); 
        return blogs.stream()
                .map(blog -> {
                    BlogDto dto = new BlogDto();
                    // Copy properties to the DTO
                    BeanUtils.copyProperties(blog, dto); 
                    return dto;
                })
                .collect(Collectors.toList()); // Collect the DTOs into a list
    }

    // Method to delete a BlogPost by its ID
    @Override
    public void deleteBlog(Long id) {
    	// Delete the BlogPost from the repository by its ID
        blogRepository.deleteById(id); 
    }



}
