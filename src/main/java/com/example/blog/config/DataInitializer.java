package com.example.blog.config;

import com.example.blog.dto.Blog.BlogDto;
import com.example.blog.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataInitializer implements CommandLineRunner {
    private final BlogService blogService;

    @Autowired
    public DataInitializer(BlogService blogService) {
        this.blogService = blogService;
    }


    @Override
    public void run(String... args) throws Exception {

        for (int i = 0; i < 5; i++) {
            BlogDto blog = new BlogDto();
            blog.setTitle("Blog " + (i + 1));
            blog.setAuthor("Author " + (i + 1));
            blog.setContent("Content " + (i + 1));
            blogService.createBlog(blog);
        }

    }
}
