package com.example.blog.model;

import java.time.LocalDate;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Entity
@Data
public class BlogPost {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank(message = "Title is required")
	@Column(nullable = false)
	private String title;

	@NotBlank(message = "Content is required")
	@Column(nullable = false, columnDefinition = "TEXT")
	private String content;

	@NotBlank(message = "Author is required")
	@Column(nullable = false)
	private String author;

	@Column(nullable = false)
	private LocalDate publicationDate;
}
